const multiparty = require('multiparty')
const fs = require('fs');
const { updateProfileImage, updateProfileBanner } = require('./mysql/users')
const { openConnection, closeConnection } = require("./mysql/connection_wordpress")

const saveFile = async (file, folder = "random") => {
  const filename = `public/${folder}/${new Date().getTime() + "." + file.originalFilename.split('.')[1]}`
  const data = fs.readFileSync(file.path);
  fs.writeFileSync(filename, data);
  await fs.unlinkSync(file.path);
  return filename
};

const _createProfileImage = async (req, res) => {
  const conn = await openConnection()
  try {
    let form = new multiparty.Form();
    let FormResp = await new Promise((resolve, reject)=>{
      form.parse(req, (err,fields,files) => {
        if(err){
          reject(err)
        }
        resolve({fields,files})
      });
    });

    const {fields, files} = FormResp;
    const filename = await saveFile(files.file[0], 'profile');

    const id = await updateProfileImage(conn, req.params.id, filename)

    closeConnection(conn)
    res.status(200).send(filename)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _createProfileBanner = async (req, res) => {
  const conn = await openConnection()
  try {
    let form = new multiparty.Form();
    let FormResp = await new Promise((resolve, reject)=>{
      form.parse(req, (err,fields,files) => {
        if(err){
          reject(err)
        }
        resolve({fields,files})
      });
    });

    const {fields, files} = FormResp;
    const filename = await saveFile(files.file[0], 'banner');

    const id = await updateProfileBanner(conn, req.params.id, filename)

    closeConnection(conn)
    res.status(200).send(filename)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

module.exports = {
  _createProfileImage,
  _createProfileBanner
}