const USERS_TABLENAME = "users"

const login = async (connection, username, password) => {
  try {
    
    const SQL = `
      SELECT users.username, users.full_name, users.id, customers.id as customer_id, password, profile_img_url, banner_img_url
      FROM ${USERS_TABLENAME} users
      LEFT JOIN customers ON users.customer = customers.id
      WHERE users.username = ? AND users.status = ?
    `
    const user = await connection.awaitQuery(SQL, [username, "active"]);

    if(password) {
      if(user[0].password != password){
        throw new Error("Contraseña invalida")
      }
    }else{
      if(user[0] && user[0]?.id ==  1){
        return {
          require_password: true
        }
      }
    }

    return user[0] || null
  }catch(err){
    console.error(err)
    return JSON.stringify(err)
  }
}

module.exports = {
  login,
  USERS_TABLENAME
}