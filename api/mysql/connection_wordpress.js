const mysql = require('mysql-await')
require('dotenv').config()

const config = {
    host: process.env.MYSQL_IP,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DB
}

const DATABASE_URL = `mysql://${config.user}:${config.password}@${config.host}/${config.database}?ssl={"rejectUnauthorized":true}`

const pool = mysql.createPool(DATABASE_URL);

/*const pool = mysql.createPool({
    host: '66.225.201.168',
    user: 'zpnaexzk_designer',
    password: '%cEY+$s{%TNx',
    database: 'zpnaexzk_edithor'
});*/

/*const pool = mysql.createPool({
    host: '66.225.201.168',
    user: 'zpnaexzk_designer',
    password: '%cEY+$s{%TNx',
    database: 'zpnaexzk_wp769'
});*/

async function openConnection(){
    return await pool.awaitGetConnection()
}

function closeConnection(con) {
    con.release()
}

module.exports = {
    openConnection,
    closeConnection
}