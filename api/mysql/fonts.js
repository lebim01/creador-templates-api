const getFonts = async (connection, admin) => {
  try {
    const SQL = `
      SELECT *
      FROM fonts 
    `
    const data = await connection.awaitQuery(SQL);
    return data
  }catch(err){
    console.error(err)
    return []
  }
}

const getFont = async (connection, font_id) => {
  try {
    const SQL = `
      SELECT *
      FROM fonts
      WHERE id = ?
    `
    const data = await connection.awaitQuery(SQL, ['active', font_id]);

    return data[0]
  }catch(err){
    console.error(err)
    throw err;
  }
}

const deleteFont = async (connection, font_id) => {
  try {
    const SQL = `
      DELETE FROM fonts
      WHERE id = ?
    `
    const data = await connection.awaitQuery(SQL, [font_id]);
  }catch(err){
    console.error(err)
    throw err;
  }
}

const updateFont = async (connection, id, body) => {
  try {
    const SQL = `
      UPDATE fonts
      SET name = ?
      WHERE id = ?
    `
    const data = await connection.awaitQuery(SQL, [body.name, id]);
    return data
  }catch(err){
    console.error(err)
    throw err;
  }
}

const createFont = async (connection, body) => {
  try {
    const SQL = `
      INSERT INTO fonts
      SET 
        name = ?,
        file = ?,
        type = ?
    `
    const data = await connection.awaitQuery(SQL, [body.name, body.file, 'custom']);
    const id = data.insertId
    return id
  }catch(err){
    console.error(err)
    throw err;
  }
}

module.exports = {
  getFonts,
  getFont,
  deleteFont,
  updateFont,
  createFont
}
