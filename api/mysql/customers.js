const getCustomers = async (connection) => {
  try {
    const SQL = `
      SELECT 
        id,
        name
      FROM customers 
    `
    const data = await connection.awaitQuery(SQL, []);
    return data
  }catch(err){
    console.error(err)
    return []
  }
}

const createCustomer = async (connection, body) => {
  try {
    const SQL = `
      INSERT INTO customers
      SET name = ?
    `
    const data = await connection.awaitQuery(SQL, [body.name]);
    const id = data.insertId
    return id
  }catch(err){
    console.error(err)
    throw err;
  }
}

module.exports = {
  getCustomers,
  createCustomer
}