
const getCategories = async (connection, admin, customer_id) => {
  try {
    const SQL = `
      SELECT 
        categories.id,
        categories.name,
        categories.customer_id,
        customers.name as customer
      FROM categories 
      INNER JOIN customers ON categories.customer_id = customers.id
      WHERE categories.status = ?
      ORDER BY categories.index
    `
    const data = await connection.awaitQuery(SQL, ['active']);
    return data
  }catch(err){
    console.error(err)
    return []
  }
}

const getCategory = async (connection, category_id) => {
  try {
    const SQL = `
      SELECT *
      FROM categories
      WHERE id = ?
    `
    const data = await connection.awaitQuery(SQL, ['active', category_id]);

    return data[0]
  }catch(err){
    console.error(err)
    throw err;
  }
}

const deleteCategory = async (connection, category_id) => {
  try {
    const SQL = `
      DELETE FROM categories
      WHERE id = ?
    `
    const data = await connection.awaitQuery(SQL, [category_id]);
  }catch(err){
    console.error(err)
    throw err;
  }
}

const updateCategory = async (connection, id, body) => {
  try {
    const SQL = `
      UPDATE categories
      SET name = ?
      WHERE id = ?
    `
    const data = await connection.awaitQuery(SQL, [body.name, id]);
    return data
  }catch(err){
    console.error(err)
    throw err;
  }
}

const createCategory = async (connection, body) => {
  try {
    const categories = await getCategories(connection)

    const SQL = `
      INSERT INTO categories
      SET name = ?,
        customer_id = ?,
        categories.index = ?
    `
    const data = await connection.awaitQuery(SQL, [body.name, body.customer_id, categories.length]);
    const id = data.insertId
    return id
  }catch(err){
    console.error(err)
    throw err;
  }
}

const moveIndex = async (connection, id, index) => {
  try {
    const SQL = `
      UPDATE categories
      SET categories.index = ?
      WHERE id = ?
    `
    const data = await connection.awaitQuery(SQL, [index, id]);
    return index
  }catch(err){
    console.error(err)
    throw err;
  }
}

module.exports = {
  getCategories,
  getCategory,
  deleteCategory,
  updateCategory,
  createCategory,
  moveIndex
}