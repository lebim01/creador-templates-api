const { USERS_TABLENAME } = require("./auth");

const updateProfileImage = async (conn, user_id, filename) => {
  try {
    const SQL = `
      UPDATE ${USERS_TABLENAME}
      SET profile_img_url = ?
      WHERE ID = ?
    `
    console.log(SQL, [filename, user_id])
    const data = await conn.awaitQuery(SQL, [filename, user_id]);
    return data
  }catch(err){
    console.error(err)
    return []
  }
}

const updateProfileBanner = async (conn, user_id, filename) => {
  try {
    const SQL = `
      UPDATE ${USERS_TABLENAME}
      SET banner_img_url = ?
      WHERE ID = ?
    `
    const data = await conn.awaitQuery(SQL, [filename, user_id]);
    return data
  }catch(err){
    console.error(err)
    return []
  }
}

module.exports = {
  updateProfileImage,
  updateProfileBanner
}