const getTemplates = async (connection, admin) => {
  try {
    const SQL = `
      SELECT 
        template.id,
        template.name,
        template.category_id,
        categories.customer_id
      FROM template 
      INNER JOIN categories ON template.category_id = categories.id
      WHERE template.status = ? ${!admin ? "AND template.privacity = ?" : ""}
    `
    const templates = await connection.awaitQuery(SQL, ['active', 'public']);
    return templates
  }catch(err){
    console.error(err)
    return []
  }
}

const getTemplateItems = async (connection, template_id) => {
  try {
    const SQL = `
      SELECT * 
      FROM template_items 
      WHERE template_id = ?
      ORDER BY _index
    `
    const items = await connection.awaitQuery(SQL, [template_id]);

    if(items.length > 0){
      for(const item of items){
        item.position = {
          left: item.position_left,
          top: item.position_top
        }
    
        item.dimension = {
          height: item.dimension_height,
          width: item.dimension_width
        }
      }
    }

    return items
  }catch(err){
    console.error(err)
    return []
  }
}

const getTemplate = async (connection, template_id) => {
  try {
    const SQL = `
      SELECT id, category_id, created_at, name, privacity, status
      FROM template
      WHERE status = ? AND id = ?
    `
    const templates = await connection.awaitQuery(SQL, ['active', template_id]);

    if(templates.length > 0){
      for(const template of templates){
        template.items = await getTemplateItems(connection, template.id)
      }
    }

    return templates[0]
  }catch(err){
    console.error(err)
    return null
  }
}

const createTemplate = async (connection, body) => {
  try {
    const SQL = `
      INSERT INTO template
      SET
        name = ?,
        privacity = ?,
        category_id = ?
    `
    const template = await connection.awaitQuery(SQL, [body.name, 'public', body.category_id]);
    const id = template.insertId
    return id
  }catch(err){
    console.error(err)
    return null
  }
}

const updateTemplate = async (connection, body) => {
  try {
    const SQL = `
      UPDATE template
      SET
        name = ?,
        privacity = ?,
        category_id = ?
      WHERE id = ?
    `
    await connection.awaitQuery(SQL, [body.name, 'public', body.category_id, body.id]);

    await connection.awaitQuery(`
      DELETE FROM template_items
      WHERE template_id = ? AND mode = ?
    `, [
      body.id,
      body.mode
    ])

    return body.id
  }catch(err){
    console.error(err)
    return null
  }
}

const deleteTemplate = async (connection, template_id) => {
  try {
    const SQL = `
      DELETE FROM template
      WHERE id = ?
    `
    await connection.awaitQuery(SQL, [template_id]);

    const SQL2 = `
      DELETE FROM template_items
      WHERE template_id = ?
    `
    await connection.awaitQuery(SQL2, [template_id]);

    return true
  }catch(err){
    console.error(err)
    throw err;
  }
}

const getPreview = async (connection, template_id) => {
  try {
    const SQL = `
      SELECT 
        preview_image_post_tiny AS preview_image_post,
        preview_image_history_tiny AS preview_image_history
      FROM template
      WHERE status = ? AND id = ?
    `
    const templates = await connection.awaitQuery(SQL, ['active', template_id]);

    return templates[0]
  }catch(err){
    console.error(err)
    return null
  }
}

const addItemTemplate = async (connection, template_id, item) => {
  const SQL = `
    INSERT INTO template_items
    SET
      template_id = ?,
      content = ?,
      type = ?,
      position_left = ?,
      position_top = ?,
      dimension_width = ?,
      dimension_height = ?,
      _index = ?,
      field_name = ?,
      editable = ?,
      uuid = ?,
      principal = ?,
      mode = ?,
      note = ?,
      rotation = ?
  `
  await connection.awaitQuery(SQL, [
    template_id, 
    item.content,
    item.type, 
    item.position_left, 
    item.position_top, 
    item.dimension_width, 
    item.dimension_height, 
    item._index, 
    item.field_name || "", 
    item.editable ? 1 : 0, 
    item.uuid,
    item.principal ? 1 : 0,
    item.mode,
    item.note,
    item.rotation
  ]);
}

const updateTemplatePreview = async (connection, id, mode, preview_image) => {
  try {
    const SQL = `
      UPDATE template
      SET
        preview_image_${mode} = ?
      WHERE id = ?
    `
    await connection.awaitQuery(SQL, [preview_image, id]);

    return id
  }catch(err){
    console.error(err)
    return null
  }
}

const updateTemplatePreviewTiny = async (connection, id, mode, preview_image) => {
  try {
    const SQL = `
      UPDATE template
      SET
        preview_image_${mode}_tiny = ?
      WHERE id = ?
    `
    await connection.awaitQuery(SQL, [preview_image, id]);

    return id
  }catch(err){
    console.error(err)
    return null
  }
}

const deleteMode = async (connection, id, mode) => {
  try {
    const SQL = `
      DELETE FROM template_items
      WHERE template_id = ? AND mode = ?
    `
    await connection.awaitQuery(SQL, [id, mode]);

    const SQL2 = `
      UPDATE template
      SET preview_image_${mode} = NULL
      WHERE id =
    `
    await connection.awaitQuery(SQL2, [id]);

    return id
  }catch(err){
    console.error(err)
    return null
  }
}

const duplicateTemplate = async (connection, template_id) => {
  const SQL = `
    INSERT INTO template (name, category_id, privacity, status, preview_image_post, preview_image_history, preview_image_post_tiny, preview_image_history_tiny)
    SELECT name, category_id, privacity, status, preview_image_post, preview_image_history, preview_image_post_tiny, preview_image_history_tiny
    FROM template
    WHERE id = ?
  `
  const template = await connection.awaitQuery(SQL, [template_id]);

  const SQL2 = `
    INSERT INTO template_items (mode, template_id, type, content, position_left, position_top, dimension_width, dimension_height, _index, field_name, editable, uuid, principal, note, rotation)
    SELECT mode, ?, type, content, position_left, position_top, dimension_width, dimension_height, _index, field_name, editable, uuid, principal, note, rotation
    FROM template_items
    WHERE template_id = ?
  `
  await connection.awaitQuery(SQL2, [template.insertId, template_id]);

  return template.insertId
}

module.exports = {
  addItemTemplate,
  updateTemplate,
  getPreview,
  getTemplate,
  getTemplateItems,
  getTemplates,
  deleteTemplate,
  createTemplate,
  updateTemplatePreview,
  updateTemplatePreviewTiny,
  deleteMode,
  duplicateTemplate
}