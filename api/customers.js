const { openConnection, closeConnection } = require("./mysql/connection_wordpress")
const { getCustomers, createCustomer } = require("./mysql/customers")

const _getCustomers = async (req, res) => {
  const conn = await openConnection()
  try {
    const data = await getCustomers(conn, req.query.admin == "1", req.customer_id)
    closeConnection(conn)
    res.status(200).json(data || [])
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _createCustomer = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await createCustomer(conn, req.body)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

module.exports = {
  _getCustomers,
  _createCustomer
}