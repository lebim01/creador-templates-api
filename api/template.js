const { openConnection, closeConnection } = require("./mysql/connection_wordpress")
const { getTemplates, getTemplate, createTemplate, deleteTemplate, updateTemplate, addItemTemplate, getPreview, updateTemplatePreview, updateTemplatePreviewTiny, deleteMode, duplicateTemplate } = require("./mysql/template")

const _getTemplates = async (req, res) => {
  const conn = await openConnection()
  try {
    const courses = await getTemplates(conn, req.query.admin == "1")
    closeConnection(conn)
    res.status(200).json(courses || [])
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _createTemplate = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await createTemplate(conn, req.body.template)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _getTemplate = async (req, res) => {
  const conn = await openConnection()
  try {
    const course = await getTemplate(conn, req.params.id)
    closeConnection(conn)
    res.status(200).json(course)
  }catch(err){
    closeConnection(conn)
    res.json(err)
    res.status(500).end()
  }
}

const _deleteTemplate = async (req, res) => {
  const conn = await openConnection()
  const course = await deleteTemplate(conn, req.params.id)
  closeConnection(conn)
  res.status(200).send("Deleted")
}

const _editTemplate = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await updateTemplate(conn, req.body.template, req.body.items)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _editTemplatePreview = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await updateTemplatePreview(conn, req.params.id, req.body.mode, req.body.preview_image)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _editTemplatePreviewTiny = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await updateTemplatePreviewTiny(conn, req.params.id, req.body.mode, req.body.preview_image_tiny)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _addItem = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await addItemTemplate(conn, req.params.id, req.body)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _getTemplatePreview = async (req, res) => {
  const conn = await openConnection()
  const course = await getPreview(conn, req.params.id)
  closeConnection(conn)
  res.status(200).json(course)
}

const _deleteMode = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await deleteMode(conn, req.params.id, req.params.mode)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _duplicateTemplate = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await duplicateTemplate(conn, req.params.id)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

module.exports = {
  _getTemplate,
  _getTemplates,
  _getTemplatePreview,
  _addItem,
  _createTemplate,
  _deleteTemplate,
  _editTemplate,
  _editTemplatePreview,
  _editTemplatePreviewTiny,
  _deleteMode,
  _duplicateTemplate
}