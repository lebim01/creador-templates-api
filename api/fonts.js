const { getFont, getFonts, updateFont, createFont, deleteFont } = require('./mysql/fonts')
const multiparty = require('multiparty')
const fs = require('fs');
const { openConnection, closeConnection } = require('./mysql/connection_wordpress');

const saveFile = async (file) => {
  const data = fs.readFileSync(file.path);
  fs.writeFileSync(`./public/fonts/${file.originalFilename}`, data);
  await fs.unlinkSync(file.path);
};

const _getFonts = async (req, res) => {
  const conn = await openConnection()
  try {
    const data = await getFonts(conn, req.query.admin == "1")
    closeConnection(conn)
    res.status(200).json(data || [])
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _createFont = async (req, res) => {
  const conn = await openConnection()
  try {
    let form = new multiparty.Form();
    let FormResp = await new Promise((resolve, reject)=>{
      form.parse(req, (err,fields,files) => {
        if(err){
          reject(err)
        }
        resolve({fields,files})
      });
    });

    const {fields, files} = FormResp;
    await saveFile(files.file[0]);

    const id = await createFont(conn, {
      name: fields.name[0],
      file: files.file[0].originalFilename
    })

    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _getFont = async (req, res) => {
  const conn = await openConnection()
  const course = await getFont(conn, req.params.id)
  closeConnection(conn)
  res.status(200).json(course)
}

const _deleteFont = async (req, res) => {
  const conn = await openConnection()
  const course = await deleteFont(conn, req.params.id)
  closeConnection(conn)
  res.status(200).send("Deleted")
}

const _editFont = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await updateFont(conn, req.params.id, req.body)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

module.exports = {
  _createFont,
  _editFont,
  _deleteFont,
  _getFont,
  _getFonts
}