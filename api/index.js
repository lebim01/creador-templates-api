const path = require('path')
const express = require('express');
const { _getTemplates, _createTemplate, _getTemplate, _editTemplate, _deleteTemplate, _getTemplatePreview, _addItem, _editTemplatePreview, _editTemplatePreviewTiny, _deleteMode, _duplicateTemplate } = require('./template');
const { _createFont, _getFonts, _getFont, _editFont, _deleteFont } = require('./fonts');
const { _getCategories, _createCategory, _getCategory, _editCategory, _deleteCategory, _moveIndex } = require('./categories');
const { _createProfileBanner, _createProfileImage } = require('./users')
const { login } = require("./mysql/auth")
const cors = require('cors')

const bodyParser = require('body-parser');
const { openConnection, closeConnection } = require('./mysql/connection_wordpress');
const { _getCustomers, _createCustomer } = require('./customers');

const app = express();

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

const jsonParser = bodyParser.json()

app.use("/public", express.static(path.join(__dirname, "/public"), {
  setHeaders: (res) => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('x-timestamp', new Date())
    res.type('font/ttf')
    res.type('font/otf')
  }
}))

app.use(cors())

app.get('/', (req, res) => res.send("Welcome /"))

app.get('/api/', (req, res) => res.send("Welcome /api"))

app.post('/login', jsonParser, async function(req, res){
  const conn = await openConnection()
  try {
    const user = await login(conn, req.body.username, req.body?.password)
    closeConnection(conn)
    if(user){
      res.status(200).json(user)
    }else{
      res.status(400).end()
    }
  }catch(err){
    closeConnection(conn)
    res.status(400).json(JSON.stringify(err))
  }
})

/**
 * TEMPLATES
 */
app.get('/template', jsonParser, async function (req, res) {
  await _getTemplates(req, res)
});

app.post('/template', jsonParser, async function (req, res) {
  await _createTemplate(req, res)
});

app.get('/template/:id', jsonParser, async function (req, res) {
  await _getTemplate(req, res)
});

app.post('/template/:id/preview', jsonParser, async function (req, res) {
  await _editTemplatePreview(req, res)
});

app.post('/template/:id/preview-tiny', jsonParser, async function (req, res) {
  await _editTemplatePreviewTiny(req, res)
});

app.post('/template/:id/duplicate', jsonParser, async function (req, res) {
  await _duplicateTemplate(req, res)
});

app.post('/template/:id', jsonParser, async function (req, res) {
  await _editTemplate(req, res)
});

app.delete('/template/:id', jsonParser, async function (req, res) {
  await _deleteTemplate(req, res)
});

app.delete('/template/:id/mode/:mode', jsonParser, async function (req, res) {
  await _deleteMode(req, res)
});

app.get('/template/:id/preview', jsonParser, async function (req, res) {
  await _getTemplatePreview(req, res)
});

app.post('/template/:id/item', jsonParser, async function (req, res) {
  await _addItem(req, res)
});

/** FONTS */
app.get('/font', jsonParser, async function (req, res) {
  await _getFonts(req, res)
});

app.post('/font', async function (req, res) {
  await _createFont(req, res)
});

app.get('/font/:id', jsonParser, async function (req, res) {
  await _getFont(req, res)
});

app.post('/font/:id', jsonParser, async function (req, res) {
  await _editFont(req, res)
});

app.delete('/font/:id', jsonParser, async function (req, res) {
  await _deleteFont(req, res)
});


/** CATEGORIES */
app.get('/category', jsonParser, async function (req, res) {
  await _getCategories(req, res)
});

app.post('/category', jsonParser, async function (req, res) {
  await _createCategory(req, res)
});

app.get('/category/:id', jsonParser, async function (req, res) {
  await _getCategory(req, res)
});

app.post('/category/:id', jsonParser, async function (req, res) {
  await _editCategory(req, res)
});

app.delete('/category/:id', jsonParser, async function (req, res) {
  await _deleteCategory(req, res)
});

app.post('/category/:id/move', jsonParser, async function (req, res) {
  await _moveIndex(req, res)
});

app.get('/customer', jsonParser, async function (req, res) {
  await _getCustomers(req, res)
});

app.post('/customer', jsonParser, async function (req, res) {
  await _createCustomer(req, res)
});

app.post('/user/:id/profile', async function(req, res) {
  await _createProfileImage(req, res)
})

app.post('/user/:id/banner', async function(req, res) {
  await _createProfileBanner(req, res)
})

module.exports = app

//app.listen(3000, () => console.log("runing"))