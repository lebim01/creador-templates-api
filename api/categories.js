const { getCategories, getCategory, deleteCategory, updateCategory, createCategory, moveIndex } = require('./mysql/categories')
const { openConnection, closeConnection } = require('./mysql/connection_wordpress')

const _getCategories = async (req, res) => {
  const conn = await openConnection()
  try {
    const data = await getCategories(conn, req.query.admin == "1", req.customer_id)
    closeConnection(conn)
    res.status(200).json(data || [])
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _createCategory = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await createCategory(conn, req.body)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _getCategory = async (req, res) => {
  const conn = await openConnection()
  const course = await getCategory(conn, req.params.id)
  closeConnection(conn)
  res.status(200).json(course)
}

const _deleteCategory = async (req, res) => {
  const conn = await openConnection()
  const course = await deleteCategory(conn, req.params.id)
  closeConnection(conn)
  res.status(200).send("Deleted")
}

const _editCategory = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await updateCategory(conn, req.params.id, req.body)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

const _moveIndex = async (req, res) => {
  const conn = await openConnection()
  try {
    const id = await moveIndex(conn, req.params.id, req.body.index)
    closeConnection(conn)
    res.status(200).json(id || null)
  } catch (err) {
    closeConnection(conn)
    res.json(err);
    res.status(500).end()
  }
}

module.exports = {
  _getCategories,
  _getCategory,
  _createCategory,
  _deleteCategory,
  _editCategory,
  _moveIndex
}